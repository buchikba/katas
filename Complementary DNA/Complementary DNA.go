package kata

import "strings"

func DNAStrand(dna string) string {
	return (strings.NewReplacer("A", "T", "T", "A", "C", "G", "G", "C")).Replace(dna)
}