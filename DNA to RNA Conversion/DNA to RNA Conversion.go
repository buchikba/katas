package kata

import "strings"

func DNAtoRNA(dna string) string {
	dna = strings.Replace(dna, "T", "U", -1)
	return dna
}