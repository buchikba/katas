package kata

import "strings"

func MovingShift(s string, shift int) []string {
	shiftCount := shift - 1
	shiftrune := func(r rune) rune {
		shiftCount++
		runeWithShift := r + rune(shiftCount)
		switch {
		case r >= 'A' && r <= 'Z':
			if runeWithShift > 'Z' {
				for runeWithShift > 'Z' {
					runeWithShift -= 26
				}
			}
			return runeWithShift

		case r >= 'a' && r <= 'z':
			if runeWithShift > 'z' {
				for runeWithShift > 'z' {
					runeWithShift -= 26
				}
			}
			return runeWithShift
		}
		return r
	}
	encryptedStr := strings.Map(shiftrune, s)

	result := []string{"", "", "", "", ""}
	var resultCapacity, cnt int
	if len(encryptedStr)%5 == 0 {
		resultCapacity = len(encryptedStr) / 5
	} else {
		resultCapacity = len(encryptedStr)/5 + 1
	}

	for j := range result {
		endOfStr := cnt + resultCapacity
		if endOfStr > len(encryptedStr) {
			endOfStr = len(encryptedStr)
		}
		result[j] = encryptedStr[cnt:endOfStr]
		cnt = endOfStr
	}
	return result
}

func DemovingShift(arr []string, shift int) string {
	var result string
	for j := range arr {
		result += arr[j]
	}
	shiftCount := shift - 1
	shiftrune := func(r rune) rune {
		shiftCount++
		runeWithShift := r - rune(shiftCount)
		switch {
		case r >= 'A' && r <= 'Z':
			if runeWithShift < 'A' {
				for runeWithShift < 'A' {
					runeWithShift += 26
				}
			}
			return runeWithShift
		case r >= 'a' && r <= 'z':
			if runeWithShift < 'a' {
				for runeWithShift < 'a' {
					runeWithShift += 26
				}
			}
			return runeWithShift
		}
		return r
	}
	result = strings.Map(shiftrune, result)
	return result
}