package kata

func Gap(g, m, n int) []int {
	sieve := make([]bool, n)
	previousPrime := 0
	for i := 2; i < n; i++ {
		if sieve[i] == true {
			continue
		}
		for k := i * i; k < n; k += i {
			sieve[k] = true
		}
		if previousPrime >= m && i-previousPrime == g {
			return []int{previousPrime, i}
		}
		previousPrime = i
	}
	return nil
}