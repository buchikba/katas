package kata

func MxDifLg(a1 []string, a2 []string) int {
	if cap(a1) != 0 && cap(a2) != 0 {
		var max1, max2 int
		min1, min2 := len(a1[len(a1)-1]), len(a2[len(a2)-1])
		for i := range a1 {
			if len(a1[i]) > max1 {
				max1 = len(a1[i])
			}
			if len(a1[i]) < min1 {
				min1 = len(a1[i])
			}
		}
		for i := range a2 {
			if len(a2[i]) > max2 {
				max2 = len(a2[i])
			}
			if len(a2[i]) < min2 {
				min2 = len(a2[i])
			}
		}
		if (max1 - min2) > (max2 - min1) {
			return max1 - min2
		} else {
			return max2 - min1
		}
	} else {
		return -1
	}
}