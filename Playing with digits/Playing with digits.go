package kata

import (
	"math"
	"strconv"
)

func DigPow(n, p int) int {
	str := strconv.Itoa(n)
	sum := 0.0
	for i := 1; i < len(str)+1; i++ {
		in, _ := strconv.Atoi(str[i-1 : i])
		sum += math.Pow(float64(in), float64(p))
		p++
	}
	if int(sum)%n != 0 {
		return -1
	} else {
		return int(sum) / n
	}
}