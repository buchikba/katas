package kata

import (
	"fmt"
	"sort"
	"strings"
	"time"
)

func Stati(strg string) string {
	if strg == "" {
		return ""
	}
	var count int
	strToDuration := func(r rune) rune {
		if r == '|' {
			count++
			if count%2 != 0 {
				r = 'h'
			} else {
				r = 'm'
			}
		}
		if r == ',' {
			r = 's'
		}
		return r
	}
	strg = strings.Map(strToDuration, strg+"s")
	strArr := strings.Fields(strg)
	nanoSecondsArr := make([]time.Duration, 0, len(strArr))
	var resultAverage time.Duration
	for i := range strArr {
		hmsDuration, _ := time.ParseDuration(strArr[i])
		nanoSecondsArr = append(nanoSecondsArr, hmsDuration)
		resultAverage += hmsDuration
	}
	sort.Slice(nanoSecondsArr, func(i, j int) bool {
		return nanoSecondsArr[i].Nanoseconds() < nanoSecondsArr[j].Nanoseconds()
	})
	resultRange := nanoSecondsArr[len(nanoSecondsArr)-1] - nanoSecondsArr[0]
	resultAverage = time.Duration(resultAverage.Nanoseconds() / int64(len(nanoSecondsArr)))
	resultMedian := nanoSecondsArr[len(nanoSecondsArr)/2]
	if len(nanoSecondsArr)%2 == 0 {
		resultMedian = (resultMedian + nanoSecondsArr[len(nanoSecondsArr)/2-1]) / 2
	}
	durationToHMS := func(t time.Duration) string {
		h := int(t.Seconds()) / 60 / 60
		m := int(t.Seconds()) / 60 % 60
		s := int(t.Seconds()) % 60 % 60
		return fmt.Sprintf("%02d|%02d|%02d", h, m, s)
	}
	return fmt.Sprint("Range: ", durationToHMS(resultRange), " Average: ", durationToHMS(resultAverage), " Median: ", durationToHMS(resultMedian))
}